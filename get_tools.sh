GIT_DIR="${HOME}/Development/Security"

# Get backdoor factory
cd $GIT_DIR
git clone https://github.com/secretsquirrel/the-backdoor-factory
cd the-backdoor-factory && ./install.sh
cd $GIT_DIR

# Get httpscreenshot
cd $GIT_DIR
git clone https://github.com/breenmachine/httpscreenshot.git
cd httpscreenshot
chmod +x install-dependencies.sh && ./install-depenencies.sh

# Get smbexec
cd $GIT_DIR
git clone https://github.com/pentestgeek/smbexec.git
cd smbexec && ./install.sh
./install.sh

# Get masscan
cd $GIT_DIR
apt install git gcc make libpcap-dev
git clone https://github.com/robertdavidgraham/masscan.git


# Get CMSmap
git clone https://github.com/Dionach/CMSmap.git

# Get EyeWitness
git clone https://github.com/ChrisTruncer/EyeWitness.git

# Get printer exploits
git clone https://github.com/MooseDojo/praedasploit

# Get SQLMap
git clone https://github.com/sqlmapproject/sqlmap

# Get discover scripts
git clone https://github.com/leebaird/discover.git

# Get responder
git clone https://github.com/SpiderLabs/Responder.git

# Get DSHashes
wget http://ptscripts.googlecode.com/svn/trunk/dshashes.py -O /opt/NTDSXtract/dshashes.py

# Get SPARTA
git clone https://github.com/secforce/sparta.git
apt install python-elixir
apt install ldap-utils rwho rsh-client x11-apps finger
# Get NoSQLMap
git clone https://github.com/tcstool/NoSQLMap.git 

# Spiderfoot
wget http://sourceforge.net/projects/spiderfoot/files/spiderfoot-2.3.0-src.tar.gz/download
tar xzvf download
pip install lxml netaddr M2Crypto cherrypy mako

# Get mimikatz
cd $GIT_DIR
wget http://blog.gentilkiwi.com/downloads/mimikatz_trunk.zip
unzip -d ./mimikatz mimikatz_trunk.zip

# Get the social engineering toolkit
git clone https://github.com/trustedsec/social-engineer-toolkit/
cd set && ./setup.py install

cd $GIT_DIR
# Get PowerSploit
git clone https://github.com/mattifestation/PowerSploit.git
cd PowerSploit && wget https://raw.githubusercontent.com/obscuresec/random/master/StartListener.py && wget https://raw.githubusercontent.com/darkoperator/powershell_scripts/master/ps_encoder.

cd $GIT_DIR
# Get Nishang
git clone https://github.com/samratashok/nishang

# Get veil - red team toolkit for evading detection
git clone https://github.com/Veil-Framework/Veil
cd Veil && ./Install.sh -c

cd $GIT_DIR
# Get fuzzing lists
git clone https://github.com/danielmiessler/SecLists.git

# Get Net-Creds Network Parsing
git clone https://github.com/DanMcInerney/net-creds.git

# Get Wifite
git clone https://github.com/derv82/wifite

# Get WIFIPhihsher
git clone https://github.com/sophron/wifiphisher.git

# Get Phishing-Frenzy
git clone https://github.com/pentestgeek/phishing-frenzy.git /var/www/phishing-frenzy

# Get random extra phishing stuff
git clone https://github.com/macubergeek/gitlist.git

