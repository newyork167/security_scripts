import os, errno
from pathlib import Path


class DIR_FILE:
    DIR = 0
    FILE = 1
    UNKNOWN = 100


def directory_exists(dir_path):
    """Checks if the path exists and is a directory"""
    d = Path(dir_path)
    if d.is_dir():
        return d.exists()
    return False


def file_exists(file_path):
    """Checks if the path exists and is a file"""
    d = Path(file_path)
    if d.is_file():
        return d.exists()
    return False


def directory_or_file(file_dir_path):
    d = Path(file_dir_path)
    if d.is_dir():
        return DIR_FILE.DIR
    elif d.is_file():
        return DIR_FILE.FILE
    else:
        return DIR_FILE.UNKNOWN


def make_dir(dir_path):
    if not directory_exists(dir_path):
        try:
            os.makedirs(dir_path)
        except OSError as e:
            if e.errno != errno.EEXIST:
                raise
