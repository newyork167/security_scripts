import zerodaymenace as zdm

sitemap_url = "https://www.mastercard.us/en-us/sitemap.html"
output_directory = "output"
output_file = "test.txt"

if not zdm.sitemap_parser.directory.directory_exists(output_directory):
    zdm.sitemap_parser.directory.make_dir(output_directory)

zdm.sitemap_parser.save_sitemap_to(sitemap_url=sitemap_url, output_file=output_file)
