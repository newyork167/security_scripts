import requests
from bs4 import BeautifulSoup, SoupStrainer
from shellutils import directory


def get_sitemap(url):
    """ Gets sitemap from url and then parses it """
    r = requests.get(url)
    return parse_sitemap(r.text)


def parse_sitemap(sitemap_html):
    """ Parses sitemap for urls and returns a list """
    anchor_links = []
    for link in BeautifulSoup(sitemap_html, "html.parser", parse_only=SoupStrainer('a')):
        if link.has_attr('href'):
            print link['href']
            anchor_links += [link['href']]
    return anchor_links


def save_sitemap_to(sitemap_url, output_file, separator="\n"):
    """ Gets the sitemap and saves the links as a separated output file """
    links = get_sitemap(sitemap_url)

    with open(output_file, 'a' if directory.file_exists(output_file) else 'w+') as f:
        f.write(separator.join(links))
